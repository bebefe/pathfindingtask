﻿using MazeSolver.Algorithm;
using System;
using System.Collections.Generic;

namespace MazeSolver
{
    public class SimplifiedOneByOneMazeSolver : IAlgorithmExecutor
    {
        private Random random = new Random(new object().GetHashCode());
        private const int MAX_AVAILABLE_SOLO_PATH_LENGTH = 14;
        public GetPathResult Execute(PathFindingSourceData data)
        {
            var copyData = new PathFindingSourceData
            {
                Agents = data.Agents.Clone() as Point[],
                Map = data.Map.Clone() as int[,],
                PathLenght = data.PathLenght
            };
            var sequence = GenerateSequence(copyData.Agents.Length);

            return CalculateBySequence(copyData, sequence);
        }

        private GetPathResult CalculateBySequence(PathFindingSourceData data, int[] sequence)
        {
            var result = new GetPathResult(data.Agents);
            var map = new Map(data.Map);

            for (int i = 0; i < data.Agents.Length; i++)
            {
                var agent = sequence[i];

                var singleAgent = new Point[] { data.Agents[agent] };
                var partResult = data.PathLenght > MAX_AVAILABLE_SOLO_PATH_LENGTH ?
                                  new IterationSimplifiedMazeSolver().Execute(new PathFindingSourceData(data, singleAgent)) :
                                  new ConcreteStep(data.PathLenght, 0, map, singleAgent).CalculateStepResult();

                UpdateAgentResult(agent, result, partResult.PathHistory);
                UpdateMap(map, partResult.PathHistory);
                result.TotalSum += partResult.TotalSum;
            }

            return result;
        }

        private void UpdateAgentResult(int agent, GetPathResult result, IList<IList<Point>> pathHistory)
        {
            result.PathHistory[agent] = pathHistory[0];
        }

        private int[] GenerateSequence(int count)
        {
            var result = new int[count];
            for (int i = 0; i < count; i++)
            {
                result[i] = i;
            }

            for (int i = 0; i < count; i++)
            {
                var pos = random.Next(count);
                var temp = result[i];
                result[i] = result[pos];
                result[pos] = temp;
            }

            return result;
        }

        private void UpdateMap(Map map, IList<IList<Point>> pathHistory)
        {
            for (int j = 0; j < pathHistory[0].Count; j++)
            {
                map[pathHistory[0][j]] = 0;
            }
        }
    }
}