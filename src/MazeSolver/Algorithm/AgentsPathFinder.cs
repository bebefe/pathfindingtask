﻿using MazeSolver.Algorithm;
using System;
using System.Collections.Generic;
using System.Data;

namespace MazeSolver
{
    public class AgentsPathFinder
    {
        private readonly PathFindingSourceData sourceData;
        private IList<IList<Point>> answer;
        public IList<IAlgorithmExecutor> SpecificAlgorithms { get; }

        public AgentsPathFinder(PathFindingSourceData data)
        {
            sourceData = data;
            sourceData.Map = data.Map.Clone() as int[,];

            SpecificAlgorithms = new List<IAlgorithmExecutor>
            {
                new IterationSimplifiedMazeSolver(),
              //  new SimplifiedOneByOneMazeSolver()
            };
        }

        public IList<IList<Point>> GetResult(bool canUseStarstPoints)
        {
            var result = new List<IList<Point>>();
            for (int i = 0; i < answer.Count; i++)
            {
                result.Add(new List<Point>());
                var agentAnswer = answer[i];

                if (canUseStarstPoints)
                {
                    UpdateResult(result[i], agentAnswer);
                }
                else
                {
                    AddPoints(result[i], agentAnswer, agentAnswer.Count - 2, 0);
                }
            }

            return result;
        }

        private void AddPoints(IList<Point> result, IList<Point> source, int from, int to = 0)
        {
            for (int j = from; j >= to; --j)
            {
                result.Add(source[j]);
            }
        }

        private void UpdateResult(IList<Point> result, IList<Point> agentAnswer)
        {
            var lastIndex = agentAnswer.Count - 1;
            var last = agentAnswer[lastIndex];
            var isLastOnWall = sourceData.Map[last.X, last.Y] == Map.WallNumber;
            lastIndex = isLastOnWall ? lastIndex - 1 : lastIndex;

            var fromIndex = lastIndex;
            var toIndex = 0;
            if (agentAnswer.Count > 1 && agentAnswer[0].Equals(agentAnswer[1]))
            {
                toIndex = 1;
            }
            else
            {
                var canAddBothEnds = agentAnswer.Count < sourceData.PathLenght;
                if (!canAddBothEnds && !isLastOnWall)
                {
                    var firstPoint = agentAnswer[0];
                    var isLastPointGreater = sourceData.Map[last.X, last.Y] > sourceData.Map[firstPoint.X, firstPoint.Y];
                    fromIndex = isLastPointGreater ? lastIndex : lastIndex - 1;
                    toIndex = isLastPointGreater ? 1 : 0;
                }
            }

            AddPoints(result, agentAnswer, fromIndex, toIndex);
        }

        public AgentsPathFinder Execute(bool canSacrificePrecisionForComplexity)
        {
            if (canSacrificePrecisionForComplexity)
            {
                var result = new GetPathResult(sourceData.Agents);
                foreach (var algo in SpecificAlgorithms)
                {
                    result.UpdateIfLessThen(algo.Execute(sourceData));
                }

                answer = result.PathHistory;
            }
            else
            {
                answer = new BruteForceMazeSolver().Execute(sourceData).PathHistory;
            }

            return this;
        }
    }
}