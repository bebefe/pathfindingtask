﻿using System.Collections.Generic;

namespace MazeSolver
{
    public class GetPathResult
    {
        public GetPathResult()
        {
            PathHistory = new List<IList<Point>>();
        }

        public int TotalSum { get; set; }

        public IList<IList<Point>> PathHistory { get; set; }

        public GetPathResult(Point[] origin): this()
        {
            for (int i = 0; i < origin.Length; i++)
            {
                PathHistory.Add(new List<Point>() { origin[i] });
            }
        }

        internal void UpdateIfLessThen(GetPathResult other)
        {
            if (TotalSum < other.TotalSum ||
                (TotalSum == other.TotalSum &&
                PathHistory[0].Count > other.PathHistory[0].Count))
            {
                TotalSum = other.TotalSum;
                PathHistory = other.PathHistory;
            }
        }

        public void Add(GetPathResult other)
        {
            if (PathHistory.Count != other.PathHistory.Count)
                return;

            TotalSum += other.TotalSum;

            for (int agent = 0; agent < PathHistory.Count; agent++)
            {
                var agentHistory = new List<Point>(other.PathHistory[agent]);
                agentHistory.RemoveAt(agentHistory.Count - 1);
                agentHistory.AddRange(PathHistory[agent]);
                PathHistory[agent] = agentHistory;
            }
        }
    }
}