﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;

namespace MazeSolver.Algorithm
{
    public class IterationSimplifiedMazeSolver : IAlgorithmExecutor
    {
        private Map map;
        private Point[] agents;
        private static IDictionary<int, int> maxAvailableSoloLength;
        private const int MAX_AGENTS_COUNT_FOR_USE_ALL_WAYS = 4;

        static IterationSimplifiedMazeSolver()
        {
            maxAvailableSoloLength = new Dictionary<int, int>();
            maxAvailableSoloLength[1] = 10;
            maxAvailableSoloLength[2] = 6;
            maxAvailableSoloLength[3] = 5;
            maxAvailableSoloLength[4] = 4;
            maxAvailableSoloLength[5] = 3;
            maxAvailableSoloLength[8] = 2;
            maxAvailableSoloLength[9] = 1;
        }

        public GetPathResult Execute(PathFindingSourceData data)
        {
            map = new Map(data.Map);
            agents = data.Agents.Clone() as Point[];

            var maxIterationLenght = CalculateIterationLenght(agents.Length, data.PathLenght);

            if (agents.Length > MAX_AGENTS_COUNT_FOR_USE_ALL_WAYS)
            {
                var partsCount = (int)Math.Ceiling((decimal)data.PathLenght / maxIterationLenght);
                return CalculateWithOneResult(data, maxIterationLenght, partsCount);
            }

            return CalculateWithAllTopResults(data, maxIterationLenght);
        }

        private GetPathResult CalculateWithAllTopResults(PathFindingSourceData originData, int maxIterationLenght)
        {
            var stepLength = originData.PathLenght >= maxIterationLenght ?
                                maxIterationLenght : originData.PathLenght % maxIterationLenght;
            var leftLength = originData.PathLenght - stepLength;

            var allStepResults = new AllTopStepResults();
            allStepResults.Ways = new List<IList<IList<Point>>>();
            var step = new ConcreteStep(stepLength, 0, map, agents);
            step.CalculateAllTopResults(allStepResults);
            //foreach (var way in allStepResults.Ways)
            //{
            //    step.AddAgentsCoordinates(way);
            //}

            if (leftLength == 0)
            {
                return new GetPathResult
                {
                    TotalSum = allStepResults.Value,
                    PathHistory = allStepResults.Ways[0]
                };
            }

            var result = new GetPathResult(agents);
            var agentsCount = originData.Agents.Length;

            foreach (var way in allStepResults.Ways)
            {
                if (way.Count != agentsCount)
                    continue;

                var map = new Map(originData.Map);
                map.FillWithValue(way, 0);

                var agentsLastStep = new Point[agentsCount];
                for (int j = 0; j < agentsCount; j++)
                {
                    agentsLastStep[j] = way[j][0];
                }

                var oneWayData = new PathFindingSourceData(map.Base, agentsLastStep, leftLength);
                var wayResult = new IterationSimplifiedMazeSolver().Execute(oneWayData);
                result.UpdateIfLessThen(wayResult);
            }

            return result;
        }

        private GetPathResult CalculateWithOneResult(PathFindingSourceData data, int maxIterationLenght, decimal partsCount)
        {
            var result = new GetPathResult(agents);
            for (int i = 0; i < partsCount; i++)
            {
                var stepLength = data.PathLenght - i * maxIterationLenght >= maxIterationLenght ?
                                                     maxIterationLenght : data.PathLenght % maxIterationLenght;
                var partResult = new ConcreteStep(stepLength, 0, map, agents).CalculateStepResult();
                UpdateAgents(partResult.PathHistory);
                map.FillWithValue(partResult.PathHistory, 0);
                result.Add(partResult);
            }

            return result;
        }

        private void UpdateAgents(IList<IList<Point>> pathHistory)
        {
            for (int i = 0; i < agents.Length; i++)
            {
                agents[i] = pathHistory[i][0];
            }
        }

        private int CalculateIterationLenght(int agentsCount, int maxLength)
        {
            var result = 0;

            for (int i = agentsCount; i > 0; i--)
            {
                if (maxAvailableSoloLength.ContainsKey(i))
                {
                    result = maxAvailableSoloLength[i];
                    break;
                }
            }

            if (Math.Ceiling((decimal)maxLength / result) > 2 && result > 2)
                result--;

            return result;
        }
    }
}
