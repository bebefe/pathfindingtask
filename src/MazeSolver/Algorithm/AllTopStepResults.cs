﻿using System.Collections.Generic;

namespace MazeSolver
{
    public class AllTopStepResults
    {
        public int Version { get; set; }
        public int Value { get; set; }

        public IList<IList<IList<Point>>> Ways { get; set; }
    }
}