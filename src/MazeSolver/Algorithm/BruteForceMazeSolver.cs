﻿using System.Collections.Generic;

namespace MazeSolver.Algorithm
{
    public class BruteForceMazeSolver : IAlgorithmExecutor
    {
        public GetPathResult Execute(PathFindingSourceData data)
        {
            var step = new ConcreteStep(data.PathLenght, 0, new Map(data.Map), data.Agents);
            return step.CalculateStepResult();
        }
    }
}
