﻿using System.Collections.Generic;

namespace MazeSolver.Algorithm
{
    public interface IAlgorithmExecutor
    {
        GetPathResult Execute(PathFindingSourceData data);
    }
}
