﻿using System;
using System.Collections.Generic;
using System.Security.Policy;

namespace MazeSolver
{
    public class ConcreteStep
    {
        private readonly int turnsLeft;
        private int totalSum;
        private readonly Map map;
        private readonly Point[] agents;
        private int agentsCount;

        public ConcreteStep(int turnsLeft, int totalSum, Map map, Point[] agents)
        {
            this.turnsLeft = turnsLeft;
            this.totalSum = totalSum;
            this.map = map;
            this.agents = agents;
            agentsCount = agents.Length;
            UpdateMap();
        }


        public void CalculateAllTopResults(AllTopStepResults allStepResults)
        {
            var alreadyAdded = new Dictionary<int, bool>();
            PrivateCalculateAllTopResults(allStepResults, alreadyAdded);
        }

        private int PrivateCalculateAllTopResults(AllTopStepResults allStepResults, Dictionary<int, bool> alreadyAdded)
        {
            var agentsNextWays = GetNextWays();

            if (agentsNextWays == null)
            {
                var originResult = GetStartResult();
                allStepResults.Value = originResult.TotalSum;
                allStepResults.Ways = new List<IList<IList<Point>>> { originResult.PathHistory };
                return 0;
            }

            var allWays = CombineVariants(agentsNextWays);
            var lastVersion = GetAllTopResultsForWays(allWays, allStepResults, alreadyAdded);
            return lastVersion;
        }

        public GetPathResult CalculateStepResult()
        {
            var agentsNextWays = GetNextWays();

            if (agentsNextWays == null)
            {
                return GetStartResult();
            }

            var allWays = CombineVariants(agentsNextWays);
            var maxResultForWays = GetWaysResult(allWays);
            AddAgentsCoordinates(maxResultForWays.PathHistory);

            return maxResultForWays;
        }

        private IList<Point>[] GetNextWays()
        {
            if (turnsLeft <= 0)
            {
                return null;
            }

            var agentsNextWays = new IList<Point>[agentsCount];
            var hasUsefullWays = false;
            for (int i = 0; i < agentsCount; i++)
            {
                agentsNextWays[i] = map.GetUsefullNeighbors(agents[i], turnsLeft);
                hasUsefullWays = hasUsefullWays ? true : agentsNextWays[i].Count > 0;
            }

            if (!hasUsefullWays)
            {
                return null;
            }

            return agentsNextWays;
        }

        private int GetAllTopResultsForWays(IList<Point[]> allWays, AllTopStepResults maxResults, Dictionary<int, bool> alreadyAdded)
        {
            foreach (var way in allWays)
            {
                if (turnsLeft == 1)
                {
                    var stepResults = new AllTopStepResults();
                    stepResults.Ways = new List<IList<IList<Point>>>();
                    var stepResult = MakeStep(way).PrivateCalculateAllTopResults(stepResults, alreadyAdded);

                    foreach (var item in stepResults.Ways)
                    {
                        AddAgentsCoordinates(item);
                        UpdateResult(maxResults, new GetPathResult { PathHistory = item, TotalSum = stepResults.Value }, alreadyAdded);
                    }
                }
                else
                {
                    var nextStep = MakeStep(way);
                    var lastVersion = nextStep.PrivateCalculateAllTopResults(maxResults, alreadyAdded);
                    if (maxResults.Version == lastVersion)
                    {
                        if (!alreadyAdded.ContainsKey(turnsLeft))
                        {
                            foreach (var resultWay in maxResults.Ways)
                            {
                                AddAgentsCoordinates(resultWay);
                            }

                            alreadyAdded[turnsLeft] = true;
                        }
                    }
                }
            }

            return maxResults.Version;
        }

        private int UpdateResult(AllTopStepResults maxResults, GetPathResult stepResult, Dictionary<int, bool> alreadyAdded)
        {
            if (stepResult.TotalSum < maxResults.Value)
                return -1;

            if (stepResult.TotalSum == maxResults.Value)
            {
                maxResults.Ways.Add(stepResult.PathHistory);
                return maxResults.Ways.Count - 1;
            }

            maxResults.Value = stepResult.TotalSum;
            maxResults.Ways.Clear();
            maxResults.Ways.Add(stepResult.PathHistory);
            maxResults.Version++;
            alreadyAdded.Clear();
            return 0;
        }

        private GetPathResult GetWaysResult(IList<Point[]> allWays)
        {
            var maxResult = new GetPathResult { TotalSum = int.MinValue };

            foreach (var way in allWays)
            {
                var stepResult = MakeStep(way).CalculateStepResult();
                maxResult.UpdateIfLessThen(stepResult);
            }

            return maxResult;
        }

        private GetPathResult GetStartResult()
        {
            return new GetPathResult
            {
                PathHistory = AddAgentsCoordinates(new List<IList<Point>>()),
                TotalSum = totalSum
            };
        }

        private IList<Point[]> CombineVariants(IList<Point>[] ways)
        {
            AddCurrentPositionsIfNeed(ways);

            var result = new List<Point[]>();
            CalculateWaysVariants(ways, new Point[agentsCount], 0, 0, result);

            return result;
        }

        private void AddCurrentPositionsIfNeed(IList<Point>[] ways)
        {
            for (int i = 0; i < agentsCount; i++)
            {
                if (ways[i].Count == 0)
                {
                    ways[i].Add(agents[i]);
                }
            }
        }

        private void CalculateWaysVariants(IList<Point>[] ways, Point[] agentsSteps, int agent, int wayNumber, IList<Point[]> result)
        {
            if (wayNumber >= ways[agent].Count)
            {
                return;
            }

            agentsSteps[agent] = ways[agent][wayNumber];

            var agentsStep = agentsSteps.Clone() as Point[];
            CalculateWaysVariants(ways, agentsStep, agent, wayNumber + 1, result);

            var isLastAgent = agent == agentsCount - 1;
            if (isLastAgent)
            {
                result.Add(agentsSteps);
                return;
            }

            CalculateWaysVariants(ways, agentsSteps, agent + 1, 0, result);
        }

        private ConcreteStep MakeStep(Point[] stepWay)
        {
            return new ConcreteStep(turnsLeft - 1, totalSum, map.Clone(), stepWay);
        }

        public IList<IList<Point>> AddAgentsCoordinates(IList<IList<Point>> agentsWays)
        {
            var isEmpty = agentsWays.Count == 0;

            for (int agentNumber = 0; agentNumber < agents.Length; agentNumber++)
            {
                if (isEmpty)
                {
                    agentsWays.Add(new List<Point> { agents[agentNumber] });
                    continue;
                }

                var ways = agentsWays[agentNumber];
                if (ways[ways.Count - 1].Equals(agents[agentNumber]) == false)
                {
                    agentsWays[agentNumber].Add(agents[agentNumber]);
                }
            }

            return agentsWays;
        }

        private void UpdateMap()
        {
            for (int i = 0; i < agentsCount; i++)
            {
                var currentAgent = agents[i];
                if (map.IsWalkable(currentAgent))
                {
                    totalSum += map[currentAgent];
                    map[currentAgent] = 0;
                }
            }
        }
    }
}