﻿using System.IO;

namespace MazeSolver
{
    static class Input
    {
        internal static PathFindingSourceData GetInput(Stream inputStream)
        {
            using (var reader = new StreamReader(inputStream))
            {
                return new PathFindingSourceData
                {
                    Map = GetInputMap(reader),
                    Agents = GetInputAgents(reader),
                    PathLenght = GetInputPathLenght(reader)
                };
            }
        }

        private static int GetInputPathLenght(StreamReader reader)
        {
            return int.Parse(reader.ReadLine());
        }

        private static Point[] GetInputAgents(StreamReader reader)
        {
            var agentsCount = int.Parse(reader.ReadLine());
            var agents = new Point[agentsCount];
            for (var i = 0; i < agentsCount; i++)
            {
                var line = reader.ReadLine().Split(' ');
                var agentX = int.Parse(line[0]);
                var agentY = int.Parse(line[1]);
                agents[i] = new Point(agentX, agentY);
            }

            return agents;
        }

        private static int[,] GetInputMap(StreamReader reader)
        {
            var dimensionsString = reader.ReadLine().Split(' ');
            var width = int.Parse(dimensionsString[0]);
            var height = int.Parse(dimensionsString[1]);

            var map = new int[width, height];
            for (var y = 0; y < height; y++)
            {
                var line = reader.ReadLine();
                for (var x = 0; x < width; x++)
                {
                    var isWall = line[x] == '#';
                    map[x, y] = isWall ? Map.WallNumber : int.Parse(line[x].ToString());
                }
            }

            return map;
        }
    }
}
