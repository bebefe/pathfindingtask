﻿using System;
using System.Configuration;

namespace MazeSolver
{
    public class Program
    {
        public static string AssemblyPath => System.Reflection.Assembly.GetExecutingAssembly().Location;

        public static void Main(string[] args)
        {
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["NeedDebugger"]))
            {
                while (!System.Diagnostics.Debugger.IsAttached)
                    System.Threading.Thread.Sleep(100);
            }

            var input = Input.GetInput(Console.OpenStandardInput());

            if (!Convert.ToBoolean(ConfigurationManager.AppSettings["CanSpawnAgentOnWall"]))
            {
                CheckIsValidInput(input);
            }

            var solver = new AgentsPathFinder(input);
            var enableSimplifiedSolver = Convert.ToBoolean(ConfigurationManager.AppSettings["CanSacrificePrecisionForComplexity"]);
            var canUseStartingPoints = Convert.ToBoolean(ConfigurationManager.AppSettings["IsPossibleTakeStartingPointsToResult"]);
            var result = solver.Execute(enableSimplifiedSolver).GetResult(canUseStartingPoints);

            Output.WriteResult(result, Console.OpenStandardOutput());
        }

        private static void CheckIsValidInput(PathFindingSourceData input)
        {
            var testMap = new Map(input.Map);

            for (int i = 0; i < input.Agents.Length; i++)
            {
                if (testMap.IsWalkable(input.Agents[i]) == false)
                {
                    throw new ArgumentException("agent " + i + " is Not Walcable");
                }
            }
        }
    }
}