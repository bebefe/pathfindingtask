﻿using System;

namespace MazeSolver
{
    public class PathFindingSourceData
    {
        
        public PathFindingSourceData()
        {

        }
        public PathFindingSourceData(PathFindingSourceData another, Point[] agents)
        {
            Map = another.Map;
            PathLenght = another.PathLenght;
            Agents = agents;
        }

        public PathFindingSourceData(int[,] map, Point[] agents, int pathLenght)
        {
            Map = map;
            Agents = agents;
            PathLenght = pathLenght;
        }

        public int[,] Map { get; set; }

        public Point[] Agents { get; set; }

        public int PathLenght { get; set; }
    }
}
