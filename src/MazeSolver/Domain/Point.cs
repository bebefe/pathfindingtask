using System;

namespace MazeSolver
{
    public readonly struct Point
    {
        public readonly int X;
        public readonly int Y;

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override bool Equals(object obj)
        {
            if ((obj is Point) == false)
                return false;

            var p = (Point)obj;
            return X == p.X && Y == p.Y;
        }
        public override string ToString()
        {
            return $"{X} {Y}";
        }
    }
}