﻿using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace MazeSolver
{
    delegate void ActionOfTwoInt(int x, int y);
 
    public class Map
    {
        private readonly int[,] map;

        public const int WallNumber = -1;

        public Map(int[,] map)
        {
            this.map = map.Clone() as int[,];
        }

        public int[,] Base => map;

        public int this[Point coordinate]
        {
            get => map[coordinate.X, coordinate.Y];
            set
            {
                map[coordinate.X, coordinate.Y] = value;
            }
        }

        public Map Clone()
        {
            return new Map(map);
        }

        public IList<Point> GetUsefullNeighbors(Point current, int maxDepth)
        {
            var neighbours = GetWalkableNeighbors(current);
            var result = new List<Point>();

            for (int i = 0; i < neighbours.Count; i++)
            {
                if (IsUsefullWay(current, neighbours[i], maxDepth))
                {
                    result.Add(neighbours[i]);
                }
            }

            return result;
        }

        public void FillWithValue(IList<IList<Point>> points, int fillValue = 0)
        {
            for (int i = 0; i < points.Count; i++)
            {
                for (int j = 0; j < points[i].Count; j++)
                {
                    this[points[i][j]] = fillValue;
                }
            }
        }

        public IList<Point> GetWalkableNeighbors(Point current)
        {
            var result = new List<Point>();

            ActionOfTwoInt addIfPossible = (x, y) =>
            {
                var newPoint = new Point(current.X + x, current.Y + y);
                if (IsOnMap(newPoint) && IsWalkable(newPoint))
                {
                    result.Add(newPoint);
                }
            };

            addIfPossible(0, -1);
            addIfPossible(0, 1);
            addIfPossible(-1, 0);
            addIfPossible(1, 0);

            return result;
        }

        public bool IsUsefullWay(Point current, Point next, int maxDepth)
        {
            if (maxDepth <= 0)
                return false;
            if (this[next] > 0)
                return true;

            var value = this[current];
            this[current] = WallNumber;

            var result = CheckWay(next, maxDepth);

            this[current] = value;
            return result;
        }

        public bool IsOnMap(Point point)
        {
            return point.X >= 0
                   && point.X < map.GetLength(0)
                   && point.Y >= 0
                   && point.Y < map.GetLength(1);
        }

        public bool IsWalkable(Point point)
        {
            return this[point] != WallNumber;
        }

        public override string ToString()
        {
            var result = new StringBuilder();

            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    result.Append(map[j, i] == WallNumber ? "#" : map[j, i].ToString());
                }
                result.AppendLine();
            }

            return result.ToString();
        }

        private bool CheckWay(Point next, int maxDepth)
        {
            var nextPointNeighbors = GetWalkableNeighbors(next);

            foreach (var neighbour in nextPointNeighbors)
            {
                if (IsUsefullWay(next, neighbour, maxDepth - 1))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
