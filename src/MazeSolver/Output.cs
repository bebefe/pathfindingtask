﻿using System.Collections.Generic;
using System.IO;

namespace MazeSolver
{
    static class Output
    {
        internal static void WriteResult(IList<IList<Point>> result, Stream output)
        {
            using (var writer = new StreamWriter(output))
            {
                for (int agent = 0; agent < result.Count; agent++)
                {
                    var pointsStrings = new string[result[agent].Count];
                    for (int i = 0; i < pointsStrings.Length; i++)
                    {
                        pointsStrings[i] = result[agent][i].ToString();
                    }

                    var agentResult = string.Join(" ", pointsStrings);
                    writer.WriteLine(agentResult);
                }
            }
        }
    }
}
