﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Referee
{
    internal static class Program
    {
        private static readonly string[] Tasks = { "Task_Small.txt", "Task_Concurrent.txt", "Task_my.txt", "Task_Big.txt" };

        public static void Main(string[] args)
        {
            var solverPath = string.Join(" ", args);
            if(string.IsNullOrWhiteSpace(solverPath))
            {
                solverPath = MazeSolver.Program.AssemblyPath;
            }

            foreach (var task in Tasks.Skip(1))
            {
                try
                {
                    CheckTask(task, solverPath);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"{task} result in exception: " + e);
                }
            }
            Console.ReadKey();
        }

        private static void CheckTask(string task, string solverPath)
        {
            var definitionStr = File.ReadAllText(task);
            var sw = new Stopwatch();
            sw.Start();
            var answerStr = ExecuteSolver(solverPath, definitionStr);
            sw.Stop();

            var definition = Input.ParseDefinition(definitionStr);
            var answer = Input.ParseAnswer(answerStr);
            var score = Game.ScoreAnswer(definition, answer);
            Console.WriteLine($"{task} scored {score} in {sw.Elapsed:g}");
        }

        private static string ExecuteSolver(string executorPath, string taskDefinition)
        {
            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = executorPath,
                    UseShellExecute = false,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true
                }
            };
            process.Start();
            process.StandardInput.WriteLine(taskDefinition);
            process.WaitForExit();
            return process.StandardOutput.ReadToEnd();
        }
    }
}