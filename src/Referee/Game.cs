using System;
using System.Collections.Generic;

namespace Referee
{
    internal static class Game
    {
        public static int ScoreAnswer((int[,] map, Point[] agents, int pathLenght) definition, List<Point>[] answer)
        {
            var (map, agents, pathLenght) = definition;

            var totalScore = 0;
            for (var i = 0; i < agents.Length; i++)
            {
                if (i == answer.Length)
                    throw new Exception($"agent {i}: no path provided");
                var path = answer[i];
                if (path.Count > pathLenght)
                    throw new Exception($"agent {i}: path exceed max lenght");
                var agent = agents[i];
                if (!agent.IsNear(path[0]))
                    throw new Exception($"agent {i}: path doesn't start near agent");
                Point? prevPoint = null;
                foreach (var point in path)
                {
                    if (!IsOnMap(map, point))
                        throw new Exception($"agent {i}: path point {point} is not on map");
                    if (!IsWalkable(map, point))
                        throw new Exception($"agent {i}: path point {point} is not walkable");
                    if (prevPoint.HasValue && !prevPoint.Value.IsNear(point))
                        throw new Exception($"agent {i}: path point {point} is not near previous {prevPoint.Value}");
                    prevPoint = point;
                    totalScore += map[point.X, point.Y];
                    map[point.X, point.Y] = 0;
                }
            }

            return totalScore;
        }

        private static bool IsOnMap(int[,] map, Point point)
        {
            return point.X >= 0
                   && point.X < map.GetLength(0)
                   && point.Y >= 0
                   && point.Y < map.GetLength(1);
        }

        private static bool IsWalkable(int[,] map, Point point)
        {
            return map[point.X, point.Y] != -1;
        }
    }
}