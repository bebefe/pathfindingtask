using System;
using System.Collections.Generic;
using System.Linq;

namespace Referee
{
    internal static class Input
    {
        public static (int[,] map, Point[] agents, int pathLenght) ParseDefinition(string definitionStr)
        {
            var input = definitionStr.Replace("\r", string.Empty).Split('\n');
            var dimensions = input[0].Split(' ').Select(int.Parse).ToArray();
            var width = dimensions[0];
            var height = dimensions[1];
            var map = new int[width, height];
            var lineShift = 1;
            for (var y = 0; y < height; y++)
            {
                var line = input[y + lineShift];
                for (var x = 0; x < width; x++)
                {
                    if (line[x] == '#')
                        map[x, y] = -1;
                    else
                        map[x, y] = int.Parse(line[x].ToString());
                }
            }

            lineShift += height;
            var agentsCount = int.Parse(input[lineShift]);
            lineShift += 1;
            var agents = new Point[agentsCount];
            for (var i = 0; i < agentsCount; i++)
            {
                var line = input[i + lineShift].Split(' ').Select(int.Parse).ToArray();
                agents[i] = new Point(line[0], line[1]);
            }

            lineShift += agentsCount;
            var pathLenght = int.Parse(input[lineShift]);

            return (map, agents, pathLenght);
        }

        public static List<Point>[] ParseAnswer(string answerStr)
        {
            var input = answerStr
              .Replace("\r", string.Empty).Split('\n')
              .Where(x => x.Length > 0)
              .Select(x => x.Split(' ').Select(SafeParse).ToArray())
              .ToArray();
            var answer = new List<Point>[input.Length];
            for (var i = 0; i < input.Length; i++)
            {
                var line = input[i];
                if (line.Length % 2 != 0)
                    throw new Exception($"agent {i}: path contain odd number of numbers");

                var agentPath = new List<Point>();
                answer[i] = agentPath;

                for (var j = 0; j < line.Length; j += 2)
                    agentPath.Add(new Point(line[j], line[j + 1]));
            }

            return answer;

            int SafeParse(string val)
            {
                if (!int.TryParse(val, out var res))
                    throw new Exception("agent path accept only integer values, but found " + val);
                return res;
            }
        }
    }
}