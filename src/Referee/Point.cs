using System;

namespace Referee
{
    internal readonly struct Point
    {
        public readonly int X;
        public readonly int Y;

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public bool IsNear(Point p)
        {
            if (X == p.X)
                return Math.Abs(Y - p.Y) <= 1;
            if (Y == p.Y)
                return Math.Abs(X - p.X) <= 1;
            return false;
        }

        public override string ToString()
        {
            return $"{X} {Y}";
        }
    }
}